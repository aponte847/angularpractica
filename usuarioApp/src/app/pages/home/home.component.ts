import { Component, OnInit } from '@angular/core';
import { Usuario } from '../../models/usuario.interface';
import { UsuariosService } from '../../services/usuarios.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  usuarios: Usuario[];

  constructor(private usuariosService: UsuariosService) { 
    this.usuarios = [];
  }

  ngOnInit(): void {
    this.usuarios = this.usuariosService.getUsuarios();
    console.log(this.usuarios);
    
  }

}
