import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';

const ROUTES: Routes = [
 {
   path: '',
   children: [
     { path: 'home', component: HomeComponent},
     { path: '**', redirectTo: 'home'}
   ]
 }
  
];

@NgModule({
  imports: [RouterModule.forChild(ROUTES)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
