import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const ROUTES: Routes = [

  { path: 'main',
    loadChildren: () => 
      import("./pages/pages.module").then( m => m.PagesModule )
  },
  { path: 'shared',
    loadChildren: () => 
      import("./components/shared/shared.module").then( (m) => m.SharedModule )
  },
  {
    path: '**',
    redirectTo: 'main'
  }

];

@NgModule({
  imports: [RouterModule.forRoot(ROUTES)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
