import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.scss']
})
export class RegistroComponent implements OnInit {

  formRegistro!: FormGroup;

  constructor( private fb: FormBuilder) {

    this.crearFormulario();
   }

  ngOnInit(): void {
  }

  get rutNoValido() {
    return this.formRegistro.get('rut')?.invalid && this.formRegistro.get('rut')?.touched;
  }

  get nombresNoValido() {
    return this.formRegistro.get('nombres')?.invalid && this.formRegistro.get('nombres')?.touched;
  }

  get apellidosNoValido() {
    return this.formRegistro.get('apellidos')?.invalid && this.formRegistro.get('apellidos')?.touched;
  }
  
  get fechaNacimientoNoValido() {
    return this.formRegistro.get('fechaNacimiento')?.invalid && this.formRegistro.get('fechaNacimiento')?.touched;
  }

  get generoNoValido() {
    return this.formRegistro.get('genero')?.invalid && this.formRegistro.get('genero')?.touched;
  }

  crearFormulario() {
    this.formRegistro = this.fb.group({
      rut            : [  , [Validators.pattern('[0-9]{9}'), Validators.required]],
      nombres        : ['', [Validators.pattern('[a-zA-Z ]*'), Validators.required, Validators.minLength(3)]],
      apellidos      : ['', [Validators.pattern('[a-zA-Z ]*'), Validators.required, Validators.minLength(3)]],
      fechaNacimiento: ['', Validators.required],
      genero         : ['', Validators.required],
      img            : ['Img No Disponible', ]
    });
  }

  guardar() {
    console.log(this.formRegistro);

    if (this.formRegistro.invalid) {
      Object.values( this.formRegistro.controls ).forEach( control => {
        control.markAllAsTouched();
      })
    }
    
  }

  

}
