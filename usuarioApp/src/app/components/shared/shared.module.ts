import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedRoutingModule } from './shared-routing.module';
import { ReactiveFormsModule } from '@angular/forms';

import { NavbarComponent } from './navbar/navbar.component';
import { RegistroComponent } from './registro/registro.component';
import { LoginComponent } from './login/login.component';


@NgModule({
  declarations: [
    NavbarComponent,
    RegistroComponent,
    LoginComponent
  ],
  imports: [
    CommonModule, 
    SharedRoutingModule,
    ReactiveFormsModule,
  ],
  exports: [
    RegistroComponent,
    LoginComponent,
    NavbarComponent
  ]
})
export class SharedModule { }
