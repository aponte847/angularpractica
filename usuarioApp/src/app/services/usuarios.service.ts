import { Injectable } from '@angular/core';
import { Usuario } from '../models/usuario.interface';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {



  private usuarios: Usuario[] = [
    {
      rut: 25679469-1,
      nombres: "Edwin Antonio",
      apellidos: "Aponte Rodriguez",
      fechaNacimiento: '16-05-1984',
      genero: "hombre",
      img: "assets/img/hombre.jpg"
    },
    {
      rut: 25862248-0,
      nombres: "Aurimar del Carmen",
      apellidos: "Romero de Aponte",
      fechaNacimiento: '14-04-1989',
      genero: 'Mujer',
      img: "assets/img/mujer.jpg"
    },
    {
      rut: 25865292-4,
      nombres: "Even Antonio",
      apellidos: "Aponte Romero",
      fechaNacimiento: '29-06-2009',
      genero: "hombre",
      img: "assets/img/hombre.jpg"
    },
    {
      rut: 25865350-5,
      nombres: "Luisiana Sophia",
      apellidos: "Aponte Romero",
      fechaNacimiento: '24-09-2013',
      genero: 'Mujer',
      img: "assets/img/mujer.jpg"
    },
  ]

  constructor() {

   }

   getUsuarios(): Usuario[] {
    return this.usuarios;
   }
}
