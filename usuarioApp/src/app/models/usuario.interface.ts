export interface Usuario {
    rut            : number;
    nombres        : string;
    apellidos      : string;
    fechaNacimiento: string;
    genero         : string;
    img            : string;

}